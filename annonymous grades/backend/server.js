const express = require('express');
const bodyParser = require('body-parser');


const app = express();
app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
  next()
})



const Sequelize = require("sequelize");
const configuration = require("./configuration");

const { db } = configuration.development;

const sequelize = new Sequelize(db.database, db.username, db.password, {
  dialect: db.dialect,
  host: db.host,
  define: {
    timestamps: false
  }
});


const Team = sequelize.define('team', {
  team_id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: Sequelize.STRING,
    allowNull: true,
    validate: {
      len: [3, 60]
    }
  },
  members: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      len: [2, 60]
    }
  }
}, {
  underscored: true
});

const Project = sequelize.define('project', {
  project_id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      len: [5, 60]
    }
  },
  description: {
    type: Sequelize.TEXT,
    allowNull: true,
    validate: {
      len: [0, 200]
    }
  }
});

const Grade = sequelize.define('grade', {
  grade_id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  points: {
    type: Sequelize.INTEGER,
    allowNull: false,
    validate: {
      isNumeric: true,
      isInt: true,
      min: 1,
      max: 999
    }
  }
});

const Evaluater = sequelize.define('evaluater', {
  evaluater_id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      len: [5, 60]
    }
  },
  email: {
    type: Sequelize.STRING,
    allowNull: true,
    validate: {
      len: [5, 60]
    }
  }
});

Team.hasMany(Project);
Project.hasMany(Grade);
Project.hasMany(Evaluater);


app.get('/create', (req, res, next) => {
  sequelize.sync({ force: true })
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error));
});

app.get('/projects', (req, res, next) => {
  Project.findAll()
    .then((projects) => res.status(200).json(projects))
    .catch((error) => next(error));
});

app.get('/teams', (req, res, next) => {
  Team.findAll()
    .then((teams) => res.status(200).json(teams))
    .catch((error) => next(error));
});

app.get('/evaluaters', (req, res, next) => {
  Evaluater.findAll()
    .then((evaluaters) => res.status(200).json(evaluaters))
    .catch((error) => next(error));
});

app.get('/grades', (req, res, next) => {
  Grade.findAll()
    .then((grades) => res.status(200).json(grades))
    .catch((error) => next(error));
});

app.post('/teams', (req, res, next) => {
  Team.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error));
});

app.post('/projects', (req, res, next) => {
  Project.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error));
});

app.post('/grades', (req, res, next) => {
  Grade.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error));
});

app.post('/evaluaters', (req, res, next) => {
  Evaluater.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((error) => next(error));
});

app.post('/teams/:tid/grades', (req, res, next) => {
  Team.findById(req.params.tid)
    .then((team) => {
      if (team) {
        let grade = req.body
        grade.team_team_id = team.team_id
        return Team.create(team)
      }
      else {
        res.status(404).send('team not found')
      }
    })
    .then(() => {
      if (!res.headersSent) {
        res.status(201).send('team grade created')
      }
    })
    .catch((err) => next(err))
})

app.post('/teams/:tid/projects', (req, res, next) => {
  Team.findById(req.params.tid)
    .then((team) => {
      if (team) {
        let project = req.body
        project.teamTeamId = team.team_id
        return Team.create(team)
      }
      else {
        res.status(404).send('team not found')
      }
    })
    .then(() => {
      if (!res.headersSent) {
        res.status(201).send('team project created')
      }
    })
    .catch((err) => next(err))
});

app.get('/teams/:name', (req, res, next) => {
  Team.findAll({
    where: {
      name: req.params.name
    }
  })
    .then((teams) => res.status(200).json(teams))
    .catch((err) => next(err))
});

app.get('/projects/:tid/grades', (req, res, next) => {
  Project.findById(req.params.pid)
    .then((project) => {
      if (project) {
        return project.getGrades()
      }
      else {
        res.status(404).send('project not found')
      }
    })
    .then((grade) => {
      if (!res.headersSent) {
        res.status(200).json(grade)
      }
    })
    .catch((err) => next(err))
});

app.get('/teams/:tid/projects', (req, res, next) => {
  Team.findById(req.params.tid)
    .then((team) => {
      if (team) {
        return team.getProjects()
      }
      else {
        res.status(404).send('not found')
      }
    })
    .then((project) => {
      if (!res.headersSent) {
        res.status(200).json(project)
      }
    })
    .catch((err) => next(err))
});

app.put('/team/:id', (req, res, next) => {
  Team.findById(req.params.id)
    .then((team) => {
      if (team) {
        return team.update(req.body, { fields: ['name', 'members'] })
      }
      else {
        res.status(404).send('team not found')
      }
    })
    .then(() => {
      if (!res.headersSent) {
        res.status(201).send('team modified')
      }
    })
    .catch((err) => next(err))
});

app.put('/project/:id', (req, res, next) => {
  Project.findById(req.params.id)
    .then((project) => {
      if (project) {
        return project.update(req.body, { fields: ['name', 'description'] })
      }
      else {
        res.status(404).send('project not found')
      }
    })
    .then(() => {
      if (!res.headersSent) {
        res.status(201).send('project modified')
      }
    })
    .catch((err) => next(err))
});

app.put('/evaluater/:eid', (req, res, next) => {
  Evaluater.findById(req.params.eid)
    .then((evaluater) => {
      if (evaluater) {
        return evaluater.update(req.body, { fields: ['name', 'email'] })
      }
      else {
        res.status(404).send('evaluater not found')
      }
    })
    .then(() => {
      if (!res.headersSent) {
        res.status(201).send('evaluater modified')
      }
    })
    .catch((err) => next(err))
});


app.delete('/teams/:tid', (req, res, next) => {
  Team.findById(req.params.tid)
    .then((team) => {
      if (team) {
        return team.destroy()
      }
      else {
        res.status(404).send('team not found')
      }
    })
    .then(() => {
      if (!res.headersSent) {
        res.status(201).send('team removed')
      }
    })
    .catch((err) => next(err))
});

app.delete('/projects/:pid', (req, res, next) => {
  Project.findById(req.params.pid)
    .then((project) => {
      if (project) {
        return project.destroy()
      }
      else {
        res.status(404).send('project not found')
      }
    })
    .then(() => {
      if (!res.headersSent) {
        res.status(201).send('project removed')
      }
    })
    .catch((err) => next(err))
});

app.delete('/evaluaters/:eid', (req, res, next) => {
  Evaluater.findById(req.params.eid)
    .then((evaluater) => {
      if (evaluater) {
        return evaluater.destroy()
      }
      else {
        res.status(404).send('evaluater not found')
      }
    })
    .then(() => {
      if (!res.headersSent) {
        res.status(201).send('evaluater removed')
      }
    })
    .catch((err) => next(err))
});

app.use((err, req, res, next) => {
  console.warn(err);
  res.status(500).send('some error');
});

app.listen(8080, function () {
  console.log("Server was started");
});
