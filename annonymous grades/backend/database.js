const express = require('express')
const mysql = require('mysql')
const app = express()

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "program",
    dialect: "mysql"
});

con.connect(function (err) {
    if (err)
        throw err
    console.log("Connected!")
    con.query("CREATE DATABASE grading_application", function (err, res) {
        if (err)
            throw err
        console.log("Database created!")
    })

});

app.listen(8080, function () {
    console.log("Server was started");
});