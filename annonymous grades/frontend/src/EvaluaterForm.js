import React,{Component} from 'react'

class EvaluaterForm extends Component{
  constructor(props){
    super(props)
    this.state = {
      name : '',
      email: ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
      console.warn(this.state)
    }
  }
  render(){
    return (<div>
      Name : <input type="text" name="name" onChange={this.handleChange}/>
      Email : <input type="text" name="email" onChange={this.handleChange}/>
      <button id="add" className = "button button-add" onClick={() => this.props.onAdd({
      name: this.state.name,
      email: this.state.email
      })}>Add evaluater</button>
      
    </div>)
  }
}

export default EvaluaterForm