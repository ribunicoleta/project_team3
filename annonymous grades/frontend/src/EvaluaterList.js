import React, {Component} from 'react'
import EvaluaterStore from './EvaluaterStore'
import {EventEmitter} from 'fbemitter'
import Evaluater from './Evaluater'
import EvaluaterForm from './EvaluaterForm'

const ee = new EventEmitter()
const store = new EvaluaterStore(ee)

function addEvaluater(evaluater){
  store.addOne(evaluater)
}

function deleteEvaluater(id){
  store.deleteOne(id)
}

function saveEvaluater(id, evaluater){
  store.saveOne(id, evaluater)
}

class EvaluaterList extends Component{
  constructor(props){
    super(props)
    this.state = {
      evaluaters : [],
      detailsFor : -1,
      selected : null
    }
    this.cancelSelection = () => {
      this.setState({
        detailsFor : -1
      })
    }
    this.selectEvaluater = (evaluater) => {
      store.getOne(evaluater)
      ee.addListener('SINGLE_EVALUATER_LOAD', () => {
        this.setState({
          detailsFor : store.selected.username,
          selected : store.selected
        })
      })
    }
  }
  componentDidMount(){
    store.getAll()
    ee.addListener('EVALUATER_LOAD', () => {
      this.setState({
        evaluaters : store.content
      })
    })
  }
  render(){
    if (this.state.detailsFor === -1){
      return (
        <div>
          <div>
          <h3>List of evaluaters</h3>
          {this.state.evaluaters.map((a) => 
            <Evaluater evaluater={a} key={a.id} onDelete={deleteEvaluater} onSave={saveEvaluater} onSelect={this.selectEvaluater} />
          )}
          </div>
          <div>
            <EvaluaterForm onAdd={addEvaluater}/>
          </div>
        </div>
      )            
    }
  }
}

export default EvaluaterList