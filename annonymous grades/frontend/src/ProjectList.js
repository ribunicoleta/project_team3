import React, {Component} from 'react'
import ProjectStore from './ProjectStore'
import {EventEmitter} from 'fbemitter'
import Project from './Project'
import ProjectForm from './ProjectForm'

const ee = new EventEmitter()
const store = new ProjectStore(ee)

function addProject(project){
  store.addOne(project)
}

function deleteProject(id){
  store.deleteOne(id)
}

function saveProject(id, project){
  store.saveOne(id, project)
}

class ProjectList extends Component{
  constructor(props){
    super(props)
    this.state = {
      projects : [],
      detailsFor : -1,
      selected : null
    }
    this.cancelSelection = () => {
      this.setState({
        detailsFor : -1
      })
    }
    this.selectProject = (project) => {
      store.getOne(project)
      ee.addListener('SINGLE_PROJECT_LOAD', () => {
        this.setState({
          detailsFor : store.selected.name,
          selected : store.selected
        })
      })
    }
  }
  componentDidMount(){
    store.getAll()
    ee.addListener('PROJECT_LOAD', () => {
      this.setState({
        evaluaters : store.content
      })
    })
  }
  render(){
    if (this.state.detailsFor === -1){
      return (
        <div>
          <div>
          <h3>List of projects</h3>
          {this.state.evaluaters.map((a) => 
            <Project project={a} key={a.id} onDelete={deleteProject} onSave={saveProject} onSelect={this.selectProject} />
          )}
          </div>
          <div>
            <ProjectForm onAdd={addProject}/>
          </div>
        </div>
      )            
    }
  }
}

export default ProjectList