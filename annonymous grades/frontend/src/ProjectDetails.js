import React, {Component} from 'react'
import Grade from './Grade'
import GradeForm from './GradeForm'
import GradeStore from './GradeStore'
import {EventEmitter} from 'fbemitter'

const ee = new EventEmitter()
const store = new GradeStore(ee)

class ProjectDetails extends Component{
  constructor(props){
    super(props)
    this.state = {
      grades : []
    }
    this.addGrade = (grade) => {
      store.addOne(this.props.project.id, grade)
    }
    this.saveGrade = (id, grade) => {
      store.saveOne(this.props.project.id, id, grade)
    }
    this.deleteGrade = (id) => {
      store.deleteOne(this.props.project.id,id)
    }
   
  }
  componentDidMount(){
    store.getAll(this.props.project.id)
    ee.addListener('GRADE_LOAD', () => {
      this.setState({
        grades : store.content
      })
    })
  }
  render(){
    return (
      <div>
        <h2>Project details <b>{this.props.project.name}</b> </h2>
        <h3>List of grades for the project:</h3>
        {
          this.state.grades.map((p) => <Grade grade={p} onDelete={this.deleteGrade} key={p.id}  />)
        }
        <h3>Add another grade.</h3>
        <GradeForm handleAdd={this.addGrade}/>
        <input type="button" value="Back" onClick={() => this.props.onCancel()} />
      </div>  
    )
  }
}

export default ProjectDetails