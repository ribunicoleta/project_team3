import React,{Component} from 'react'

class GradeForm extends Component{
  constructor(props){
    super(props)
    this.state = {
      points : ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
      console.warn(this.state)
    }
  }
  render(){
    return (<div>
      Points : <input type="text" name="points" onChange={this.handleChange}/>
      <button id="add" className = "button button-add" onClick={() => this.props.onAdd({
      points: this.state.points
      })}>Add grade</button>
      
    </div>)
  }
}

export default GradeForm