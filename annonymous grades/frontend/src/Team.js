import React, {Component} from 'react'

class Team extends Component{
  constructor(props){
      super(props)
      console.warn(this.props)
      this.state = {
        isEditing : false,
        name : this.props.team.name,
        members : this.props.team.members
      }
      this.handleChange = (event) => {
        this.setState({
          [event.target.name] : event.target.value
        })
      }
  }
  

  render(){
    if (!this.state.isEditing){
      return (
        <div id="ite,Object">
          The team {this.props.team.name} has {this.props.members} members.
          
          <div>
            <button id="delete" className="button button-delete" onClick={() => this.props.onDelete(this.props.team)}>Delete</button>
            <button id="edit" className="button button-edit" onClick={() => this.setState({isEditing:true}) }>Edit</button>
          </div>
        </div>  
      )
    }
    else{
      return (
        <div id="edit">
            <input type="text" placeholder="Name" name="name" onChange={this.handleChange} value={this.state.name}/>
            <br/>
            <input type="text" placeholder="Members" name="members" onChange={this.handleChange} value={this.state.members}/> 
            <br/>
            
            <button id="cancel" onClick={() => this.setState({isEditing:false})}>Cancel</button>
            <button id="save" onClick={() => {this.props.onSave(this.props.evaluater.team_id, 
            {name : this.state.name, members : this.state.members}); 
                    this.setState({isEditing:true})}}>Save</button>
        </div>
      )
    }
  }
}

export default Team