import axios from 'axios'
const SERVER = 'https://2972fc0179fa4920bf9987646e887871.vfs.cloud9.us-east-2.amazonaws.com'

class EvaluaterStore{
  constructor(ee){
    this.emitter = ee
    this.content = []
    this.selected = null
  }
  getAll(){
    axios(SERVER + '/evaluaters')
      .then((response) => {
        this.content = response.data
        this.emitter.emit('EVALUATER_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  addOne(evaluater){
    axios.post(SERVER + '/evaluaters', evaluater)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  deleteOne(id){
    axios.delete(SERVER + '/evaluaters/' + id)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  saveOne(id, client){
    axios.put(SERVER + '/evaluater/' + id, client)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
}

export default EvaluaterStore