import React, {Component} from 'react'

class Evaluater extends Component{
  constructor(props){
      super(props)
      console.warn(this.props)
      this.state = {
        isEditing : false,
        name : this.props.evaluater.name,
        email : this.props.evaluater.email
      }
      this.handleChange = (event) => {
        this.setState({
          [event.target.name] : event.target.value
        })
      }
  }
  

  render(){
    if (!this.state.isEditing){
      return (
        <div id="ite,Object">
          The evaluater {this.props.evaluater.name} can be contacted at {this.props.evaluater.email} .
          
          <div>
            <button id="delete" className="button button-delete" onClick={() => this.props.onDelete(this.props.evaluater)}>Delete</button>
            <button id="edit" className="button button-edit" onClick={() => this.setState({isEditing:true}) }>Edit</button>
          </div>
        </div>  
      )
    }
    else{
      return (
        <div id="edit">
            <input type="text" placeholder="Name" name="name" onChange={this.handleChange} value={this.state.name}/>
            <br/>
            <input type="text" placeholder="Email" name="email" onChange={this.handleChange} value={this.state.email}/> 
            <br/>
            
            <button id="cancel" onClick={() => this.setState({isEditing:false})}>Cancel</button>
            <button id="save" onClick={() => {this.props.onSave(this.props.evaluater.evaluater_id, 
            {name : this.state.name, email : this.state.email}); 
                    this.setState({isEditing:true})}}>Save</button>
        </div>
      )
    }
  }
}

export default Evaluater