import React, {Component} from 'react';
import TeamStore from './TeamStore';
import {EventEmitter} from 'fbemitter';
import Team from './Team';
import TeamForm from './TeamForm';
import ProjectForm from './ProjectForm';

const ee = new EventEmitter();
const store = new TeamStore(ee);

function addTeam(team){
  store.addOne(team);
}

function deleteTeam(id){
  store.deleteOne(id);
}

function saveTeam(id, team){
  store.saveOne(id, team);
}

class TeamList extends Component{
  constructor(props){
    super(props);
    this.state = {
      teams : [],
      detailsFor : -1,
      selected : null
    };
    this.cancelSelection = () => {
      this.setState({
        detailsFor : -1
      });
    };
    this.selectTeam = (team) => {
      store.getOne(team);
      ee.addListener('SINGLE_TEAM_LOAD', () => {
        this.setState({
          detailsFor : store.selected.name,
          selected : store.selected
        });
      });
    };
  }
  componentDidMount(){
    store.getAll();
    ee.addListener('TEAM_LOAD', () => {
      this.setState({
        evaluaters : store.content
      });
    });
  }
  render(){
    if (this.state.detailsFor === -1){
      return (
        <div>
          <div>
          <h3>The teams:</h3>
          {this.state.teams.map((a) => 
            <Team team={a} key={a.id} onDelete={deleteTeam} onSave={saveTeam} onSelect={this.selectTeam} />
          )}
          </div>
          <div>
            <TeamForm onAdd={addTeam}/>
          </div>
        </div>
      );       
    }
  }
}

export default TeamList;