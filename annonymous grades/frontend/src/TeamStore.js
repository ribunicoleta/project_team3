import axios from 'axios'
const SERVER = 'https://2972fc0179fa4920bf9987646e887871.vfs.cloud9.us-east-2.amazonaws.com'

class TeamStore{
  constructor(ee){
    this.emitter = ee
    this.content = []
    this.selected = null
  }
  getAll(){
    axios(SERVER + '/teams')
      .then((response) => {
        this.content = response.data
        this.emitter.emit('TEAM_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  addOne(team){
    axios.post(SERVER + '/teams', team)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  deleteOne(id){
    axios.delete(SERVER + '/teams/' + id)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  saveOne(id, client){
    axios.put(SERVER + '/team/' + id, client)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
}

export default TeamStore