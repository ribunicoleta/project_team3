import React, { Component } from 'react';
import './App.css';
import ProjectList from './ProjectList';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Grading application</h1>
        </header>
        <ProjectList/>
      </div>
    );
  }
}

export default App;