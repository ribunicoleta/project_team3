import React, {Component} from 'react'
import GradeStore from './GradeStore'
import {EventEmitter} from 'fbemitter'
import Grade from './Grade'
import GradeForm from './GradeForm'

const ee = new EventEmitter()
const store = new GradeStore(ee)

function addGrade(grade){
  store.addOne(grade)
}

function deleteGrade(id){
  store.deleteOne(id)
}

function saveGrade(id, grade){
  store.saveOne(id, grade)
}

class GradeList extends Component{
  constructor(props){
    super(props)
    this.state = {
      grades : [],
      detailsFor : -1,
      selected : null
    }
    this.cancelSelection = () => {
      this.setState({
        detailsFor : -1
      })
    }
    this.selectGrade = (points) => {
      store.getOne(points)
      ee.addListener('SINGLE_GRADE_LOAD', () => {
        this.setState({
          detailsFor : store.selected.points,
          selected : store.selected
        })
      })
    }
  }
  componentDidMount(){
    store.getAll()
    ee.addListener('GRADE_LOAD', () => {
      this.setState({
        products : store.content
      })
    })
  }
  render(){
    if (this.state.detailsFor === -1){
      return (
        <div>
          <div>
          <h3>List of grades</h3>
          {this.state.products.map((a) => 
            <Grade grade={a} key={a.id} onDelete={deleteGrade} onSave={saveGrade} onSelect={this.selectGrade} />
          )}
          </div>
          <div>
            <ProjectForm onAdd={addGrade}/>
          </div>
        </div>
      )            
    }
  }
}

export default GradeList