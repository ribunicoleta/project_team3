  
import React, {Component} from 'react'

class Project extends Component{
  constructor(props){
      super(props)
      console.warn(this.props)
      this.state = {
        isEditing : false,
        name : this.props.project.name,
        description : this.props.project.description
      }
      this.handleChange = (event) => {
        this.setState({
          [event.target.name] : event.target.value
        })
      }
  }
  

  render(){
    if (!this.state.isEditing){
      return (
        <div id="project">
          Project {this.props.name} : {this.props.description}
          
          <div>
            <button id="delete" className="button button-delete" onClick={() => this.props.onDelete(this.props.project)}>Delete</button>
            <button id="edit" className="button button-edit" onClick={() => this.setState({isEditing:true}) }>Edit</button>
          </div>
        </div>  
      )
    }
    else{
      return (
        <div id="edit">
            <input type="text" placeholder="Name" name="project_name" onChange={this.handleChange} value={this.state.name}/>
            <br/>
            <input type="text" placeholder="Description" name="project_description" onChange={this.handleChange} value={this.state.description}/> 
            <br/>
            
            <button id="cancel" onClick={() => this.setState({isEditing:false})}>Cancel</button>
            <button id="save" onClick={() => {this.props.onSave(this.props.project.project_id, 
            {name : this.state.name, price : this.state.description}); 
                    this.setState({isEditing:true})}}>Save</button>
        </div>
      )
    }
  }
}

export default Project