import React,{Component} from 'react'

class ProjectForm extends Component{
  constructor(props){
    super(props)
    this.state = {
      name : '',
      description: ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
      console.warn(this.state)
    }
  }
  render(){
    return (<div>
      Name : <input type="text" name="name" onChange={this.handleChange}/>
      Description : <input type="text" name="description" onChange={this.handleChange}/>
      <button id="add" className = "button button-add" onClick={() => this.props.onAdd({
      name: this.state.name,
      description: this.state.description
      })}>Add project</button>
      
    </div>)
  }
}

export default ProjectForm