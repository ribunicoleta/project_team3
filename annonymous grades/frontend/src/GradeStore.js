import axios from 'axios'
const SERVER = 'https://2972fc0179fa4920bf9987646e887871.vfs.cloud9.us-east-2.amazonaws.com'

class GradeStore{
  constructor(ee){
    this.emitter = ee
    this.content = []
    this.selected = null
  }
  getAll(){
    axios(SERVER + '/grades')
      .then((response) => {
        this.content = response.data
        this.emitter.emit('GRADE_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  addOne(grade){
    axios.post(SERVER + '/grade', grade)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  deleteOne(id){
    axios.delete(SERVER + '/grade/' + id)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  saveOne(id, client){
    axios.put(SERVER + '/grade/' + id, client)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
}

export default GradeStore