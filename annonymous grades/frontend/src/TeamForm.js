import React,{Component} from 'react'

class TeamForm extends Component{
  constructor(props){
    super(props)
    this.state = {
      name : '',
      members : ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
      console.warn(this.state)
    }
  }
  render(){
    return (<div>
      Name : <input type="text" name="name" onChange={this.handleChange}/>
      Members : <input type="text" name="members" onChange={this.handleChange}/>
      <button id="add" className = "button button-add" onClick={() => this.props.onAdd({
      name: this.state.name,
      members: this.state.members
      })}>Add team</button>
      
    </div>)
  }
}

export default TeamForm