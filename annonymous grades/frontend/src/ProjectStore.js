import axios from 'axios'
const SERVER = 'https://2972fc0179fa4920bf9987646e887871.vfs.cloud9.us-east-2.amazonaws.com'

class ProjectStore{
  constructor(ee){
    this.emitter = ee
    this.content = []
    this.selected = null
  }
  getAll(){
    axios(SERVER + '/projects')
      .then((response) => {
        this.content = response.data
        this.emitter.emit('PROJECT_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  addOne(project){
    axios.post(SERVER + '/project', project)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  deleteOne(id){
    axios.delete(SERVER + '/project/' + id)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  saveOne(id, client){
    axios.put(SERVER + '/project/' + id, client)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
}

export default ProjectStore